import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/profession',
    component: Home,
    children: [
      {
        path: 'profession',
        name: "profession",
        component: () => import('../views/Profession.vue')
      },
      {
        path: 'search',
        name: "search",
        component: () => import('../views/search.vue')
      },
      {
        path: 'my',
        name: "my",
        component: () => import('../views/My.vue')
      }
    ]
  },
  {
    path:'/my/login',
    name:'login',
    component:()=>import('../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
