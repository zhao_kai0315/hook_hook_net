module.exports = {
    devServer: {
        open: true,
        host: '127.0.0.1',
        port: 8888,
        proxy: {
            // 请求的地址以lagou开头会使用代理
            "/lagou": {
                target: "https://m.lagou.com/",
                changeOrigin: true,
                pathRewrite: {
                    "^/lagou": ""
                }
            }
        }
    }
}